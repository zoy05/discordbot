package commands;

import de.btobastian.sdcf4j.Command;
import de.btobastian.sdcf4j.CommandExecutor;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.embed.EmbedBuilder;

public class testCommand implements CommandExecutor {

    @Command(aliases = {"!test"}, description = "Test command")
    public void onCommand(String[] args, TextChannel channel) {
        EmbedBuilder embed = new EmbedBuilder()
            .setTitle("It's okey.")
            .setFooter("Archie © ", "https://cdn.discordapp.com/app-icons/518175644199288832/1b24152990a0100660a59de5976d4b74.png?size=256");
        channel.sendMessage(embed);
    }

}