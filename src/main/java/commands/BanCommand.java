package commands;

import de.btobastian.sdcf4j.Command;
import de.btobastian.sdcf4j.CommandExecutor;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class BanCommand implements CommandExecutor {

    @Command(aliases = {"!ban"}, description = "Command to ban idiots", usage = "!ban <user> <reason>")
    public void onCommand(String[] args, TextChannel channel, Server server, Message message) {
        if (message.getAuthor().canBanUsersFromServer()) {
            DateFormat format = new SimpleDateFormat("MM/dd/yyyy KK:mm:ss a");
            Calendar cal = Calendar.getInstance();
            Date date = cal.getTime();
            String formattedDate = format.format(date);
            TextChannel logChannel = (TextChannel) server.getTextChannelsByName("logs");
            User toBan = (User) message.getMentionedUsers();
            server.banUser(toBan);
            String reason = "";
            for (int i = 1; i <= args.length; i++) {
                reason = reason + args[i] + " ";
            }
            EmbedBuilder embed = new EmbedBuilder()
                    .setTitle("BAN")
                    .setDescription("Ban Information")
                    .addField("User", toBan.getDisplayName(server) + "(" +toBan.getId()+ ")")
                    .addField("Ban Date", formattedDate)
                    .addField("Reason", reason)
                    .setImage("https://giphy.com/tv/search/ban");
            toBan.sendMessage("https://media.giphy.com/media/e3WNjAUKGNGoM/giphy.gif");
            logChannel.sendMessage(embed);
        }
        else {
            channel.sendMessage("I'm sorry, but I can not meet your expectations");
        }
    }

}
