package commands;

import de.btobastian.sdcf4j.Command;
import de.btobastian.sdcf4j.CommandExecutor;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;

public class botInfoCommand implements CommandExecutor {

    @Command(aliases = {"!botinfo", "!bi"}, description = "Bot info")
    public void onCommand(String[] args, Message messager, TextChannel channel) {
        EmbedBuilder embed = new EmbedBuilder()
                .setAuthor("Archie", "", "https://cdn.discordapp.com/app-icons/518175644199288832/1b24152990a0100660a59de5976d4b74.png?size=256")
                .setDescription("Bot Informations")
                .addInlineField("Bot Author", "Toby/Zoy05")
                .addInlineField("Github From The Author", "https://github.com/zoy05")
                .addInlineField("Created At", "12.03.2018")
                .addInlineField("Help Command", "!help")
                .addField("My Favourite Programming Lang is", "Scratch")
                .addField("My Love Is", "Osu Game")
                .setFooter("Archie © ", "https://cdn.discordapp.com/app-icons/518175644199288832/1b24152990a0100660a59de5976d4b74.png?size=256");
        channel.sendMessage(embed);
    }

}
