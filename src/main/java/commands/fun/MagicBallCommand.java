package commands.fun;

import de.btobastian.sdcf4j.Command;
import de.btobastian.sdcf4j.CommandExecutor;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;

import java.util.Random;

public class MagicBallCommand implements CommandExecutor {

    @Command(aliases = {"!magicball", "!mb"}, description = "This is magic ball!")
    public void onCommand(Message message, String[] args, TextChannel channel) {
        String[] answers = {
                "Yes.",
                "No.",
                "Maybe",
                "I don't know."
        };
        //-----------------------------------------------------
        Random random = new Random();
        String text = "";
        if (message.getContent().contains("?")) {
            int reply = random.nextInt(4);
            String replyString = answers[reply];

            EmbedBuilder embed = new EmbedBuilder()
                    .setTitle("Magic Ball")
                    .setDescription("Maaaagic")
                    .addField("reply", replyString)
                    .setFooter("Archie © ", "https://cdn.discordapp.com/app-icons/518175644199288832/1b24152990a0100660a59de5976d4b74.png?size=256");
            channel.sendMessage(embed);
        }
        else {
            channel.sendMessage("This is not a question.");
        }
    }

}
