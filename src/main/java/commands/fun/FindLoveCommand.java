package commands.fun;

import de.btobastian.sdcf4j.Command;
import de.btobastian.sdcf4j.CommandExecutor;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.server.Server;
import org.javacord.api.entity.user.User;

import java.util.Random;

public class FindLoveCommand implements CommandExecutor{

    @Command(aliases = {"!findlove", "!fl"}, description = "Find  your love")
    public void onCommand(String[] args, Server server, TextChannel channel) {
        User love = (User) server.getMembers().toArray()[new Random().nextInt(server.getMemberCount())];
        EmbedBuilder embed = new EmbedBuilder()
                .setTitle("Find your love")
                .setDescription("Do you know that I can foretell?")
                .addField("Your love", "<@" +love.getId()+ "> your love loves you in " + new Random().nextInt(101) + "%")
                .setFooter("Archie © ", "https://cdn.discordapp.com/app-icons/518175644199288832/1b24152990a0100660a59de5976d4b74.png?size=256");
        channel.sendMessage(embed);
    }

}
