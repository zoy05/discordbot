package commands.server;

import de.btobastian.sdcf4j.Command;
import de.btobastian.sdcf4j.CommandExecutor;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.server.Server;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class ServerInfoCommand implements CommandExecutor {

    @Command(aliases = {"!serverinfo", "!si"}, description = "Server info")
    public void onCommand(String[] args, Server server, Message message, TextChannel channel) {

        //variables created by https://gist.github.com/Mazawrath
        Date date = new Date(server.getCreationTimestamp().getEpochSecond() * 1000L);
        DateFormat format = new SimpleDateFormat("MM/dd/yyyy KK:mm:ss a");
        format.setTimeZone(TimeZone.getTimeZone("EST"));
        String formatted = format.format(date);

        EmbedBuilder embed = new EmbedBuilder()
                .setTitle(server.getName())
                .setDescription("Server Informations")
                .addInlineField("Users:", String.valueOf(server.getMemberCount()))
                .addInlineField("Server Owner:", server.getOwner().getDisplayName(server))
                .addInlineField("Roles", String.valueOf(server.getRoles().size()))
                .addInlineField("Bans", String.valueOf(server.getBans().getNumberOfDependents()))
                .addField("Afk Channel", server.getAfkChannel().get().getName())
                .addField("Created At", formatted)
                .addField("Region", server.getRegion().getName())
                .setFooter("Archie © ", "https://cdn.discordapp.com/app-icons/518175644199288832/1b24152990a0100660a59de5976d4b74.png?size=256");
        channel.sendMessage(embed);
    }

}
