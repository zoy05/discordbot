import commands.BanCommand;
import commands.botInfoCommand;
import commands.fun.FindLoveCommand;
import commands.fun.MagicBallCommand;
import commands.helpCommand;
import commands.server.ServerInfoCommand;
import commands.testCommand;
import de.btobastian.sdcf4j.CommandHandler;
import de.btobastian.sdcf4j.handler.JavacordHandler;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;

import events.*;

public class Main {

    public static void main(String[] args) {
        DiscordApi api = new DiscordApiBuilder().setToken(Options.token).login().join();

        CommandHandler cmdHandler = new JavacordHandler(api);

        //Listeners
        api.addListener(new addMessageCreateListener());
        api.addListener(new addReactionAddListener());

        //Commands
        cmdHandler.registerCommand(new testCommand());
        cmdHandler.registerCommand(new botInfoCommand());
        cmdHandler.registerCommand(new helpCommand());
        //fun
        cmdHandler.registerCommand(new MagicBallCommand());
        cmdHandler.registerCommand(new FindLoveCommand());
        //moderation
        //cmdHandler.registerCommand(new BanCommand());
        //server
        cmdHandler.registerCommand(new ServerInfoCommand());
    }

}
