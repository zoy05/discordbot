package events;

import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.listener.message.MessageCreateListener;

public class addMessageCreateListener implements MessageCreateListener {

    public void onMessageCreate(MessageCreateEvent event) {
        System.out.println(event.getMessageAuthor().getDisplayName() + ":" + event.getChannel().getId() + ":" + event.getMessage().getContent());
    }

}
